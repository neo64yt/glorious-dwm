# dwm-fuchsite

My yet another personal build of dwm (based on version 6.4) and successor of [dwm-neo64](https://github.com/neo64yt/dwm-neo64). This build is an attempt to recreate a cleaner version of its predecessor by managing & applying patches with `git patch` instead of `patch`. A part of [glorious-fuchsite](https://codeberg.org/neo64yt/glorious-fuchsite).

## Patches (currently) used in this build

+ [actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen) 
+ [alwayscenter](https://dwm.suckless.org/patches/alwayscenter)
+ [gaplessgrid](https://dwm.suckless.org/patches/gaplessgrid)
+ [nodmenu](https://dwm.suckless.org/patches/nodmenu)

## Dependencies

+ Xlib
+ libxft
+ libxinerama

## Installation guide

### 1. Cloning this repository

Run this command:

```bash
git clone https://codeberg.org/neo64yt/dwm-fuchsite.git
```

### 2. Installing dwm

You can either run the installation script (`install.sh`) or run this in the terminal:

```bash
sudo make install 
```
## Keybindings

Will be updated soon in the man page!
